Django==4.0.4
django-cors-headers==3.13.0
django-crispy-forms==1.14.0
django-downloadview==2.3.0
django-environ==0.8.1
django-rest-framework==0.1.0
djangorestframework==3.13.1
